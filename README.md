![Cube Original Image](images/cube-logo-black.png)

## Cube Responsive Theme

This repository is a custom modifications and adptative elements of original
theme: [Cube Bootstrap Admin Theme](https://wrapbootstrap.com/theme/cube-bootstrap-admin-theme-angularjs-WB008R559)
to **[Noosfero](http://gitlab.com/noosfero/noosfero)** application that use **Rails 3**
framework.

### Usage

1. With [Noosfero](http://gitlab.com/noosfero/noosfero) repository in your local folder,
enter into themes directory (`cd public/designs/themes`) and execute: `git submodule add git@gitlab.com:participa/cube-responsive-theme.git`. This command will clone this repo into `public/designs/themes` folder.
2. Run `rails server` from **noosfero** folder to starts the application.
3. Access `http://localhost:3000/admin/environment_themes` url and search by
   **Cube Responsive Theme** and click them to activate it, or follow the tutorial in
   [Noosfero - Switch to your theme](http://noosfero.org/Development/CreatingThemes#Switch_to_your_theme).
4. You sould see the modifications below:

  ![Noosfero Cube Theme](images/noosfero-cube-theme.png)
