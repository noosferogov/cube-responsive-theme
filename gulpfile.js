/**
* Noosfero cube theme tasks. See the package.json file
* the 'devDependencies' key to see the gulp modules
* used below
*/

// Import gulp plugins into variables
var gulp = require('gulp');
var plugins = require('gulp-load-plugins')({
                    pattern:['gulp-*','gulp.*','*-bower-*','run-sequence']
              });

var paths = {
   frontLibs:'./libs'
}

gulp.task('bower-install',function(){

   return plugins.bower();
});

gulp.task('bower-normalize',function(){

   //My Tasks here

   return gulp.src(plugins.mainBowerFiles(),{
                  base:'bower_components'
               })
              .pipe(plugins.plumber())
              .pipe(plugins.bowerNormalize())
              .pipe(gulp.dest(paths.frontLibs));
});

gulp.task('css',function(){

  return gulp.src([paths.frontLibs+'/sass/*.scss'])
             .pipe(plugins.plumber({errorHandler:plugins.util.log}))
             .pipe(plugins.sass())
             .pipe(plugins.minifyCss())
             .pipe(plugins.rename({suffix:'.min'}))
             .pipe(gulp.dest(paths.frontLibs+'/css'));
});

gulp.task('js',function(){

  var jsFiles = [
    paths.frontLibs+'/bootstrap/js/*.min.js',
    paths.frontLibs+'/js/*.js'
  ]

  return gulp.src(jsFiles)
             .pipe(plugins.plumber({errorHandler:plugins.util.log}))
             .pipe(plugins.uglify())
             .pipe(plugins.concat('theme.js'))
             .pipe(gulp.dest('./'))
})

gulp.task('default',function(){
    plugins.runSequence('bower-install','bower-normalize');
});

gulp.task('watch', function(){
    gulp.watch(paths.frontLibs+'/sass/**/*.scss',['css']);
    gulp.watch(paths.frontLibs+'/js/**/*.js',['js']);
});
